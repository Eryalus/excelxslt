/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excelxslt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbookType;
import org.xml.sax.InputSource;

/**
 *
 * @author eryalus
 */
public class XLSXCreator {

    private final int OTHERS_COLUMNS = LETTERS.length();
    Workbook workbook;
    private ArrayList<XLSXData> data = new ArrayList<>();
    private final String[] AVOID_TEXTUAL = new String[]{};
    private final String[] AVOID_CONTAINS = new String[]{"Update", "Security", "Hotfix", "Service Pack"};

    public XLSXCreator(ArrayList<XLSXData> data) {
        this.data = data;
    }

    public void setData(ArrayList<XLSXData> data) {
        this.data = data;
    }

    public void create(String destFile) throws IOException {

        XSSFWorkbook workbook = new XSSFWorkbook(XSSFWorkbookType.XLSX);
        int cont = 0;
        for (XLSXData dat : data) {
            XSSFSheet sheet = workbook.createSheet(dat.getSheetname());
            ArrayList<ProgramData> data = getProgramData(dat.getPath());
            addDataToSheet(sheet, data);
        }
        FileOutputStream fileOut = new FileOutputStream(destFile);
        workbook.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }

    private void addDataToSheet(XSSFSheet sheet, ArrayList<ProgramData> data) {
        ArrayList<ArrayList<String>> datos = new ArrayList<>();
        for (int i = 0; i < getMaxColum(); i++) {
            ArrayList<String> al = new ArrayList<>();
            datos.add(al);
        }
        for (ProgramData dat : data) {
            String txt = dat.toString();
            datos.get(getColumn(txt)).add(txt);
        }
        for (ArrayList<String> dat : datos) {
            Collections.sort(dat);
        }
        int max = getMax(datos);
        for (int i = 0; i < max; i++) {
            XSSFRow row = sheet.createRow(i);
            int column = 0;
            for (ArrayList<String> dat : datos) {
                column++;
                if (dat.size() <= i) {
                    continue;
                }
                String valor = dat.get(i);
                XSSFCell celda = row.createCell(column - 1);
                celda.setCellValue(valor);
            }
        }
    }

    private int getMax(ArrayList<ArrayList<String>> datos) {
        int max = 0;
        for (ArrayList<String> dat : datos) {
            max = Math.max(max, dat.size());
        }
        return max;
    }

    private ArrayList<ProgramData> getProgramData(String str) throws IOException {
        ArrayList<ProgramData> data_to_return = new ArrayList<>();
        FileInputStream inputStream = new FileInputStream(new File(str));
        Workbook libro = null;

        if (str.endsWith("xls")) {
            libro = new HSSFWorkbook(inputStream);
        } else if (str.endsWith("xlsx")) {
            libro = new XSSFWorkbook(inputStream);
        } else {
            return data_to_return;
        }

        Sheet sheet = libro.getSheetAt(0);
        Iterator<Row> iterator = sheet.iterator();
        int cont = 0;
        while (iterator.hasNext()) {
            ++cont;
            Row nextRow = iterator.next();
            if (cont < 2) {
                continue;
            }

            String name = nextRow.getCell(1).getStringCellValue();
            Double bits = nextRow.getCell(10).getNumericCellValue();
            if (!isValid(name)) {
                continue;
            }
            ProgramData dat = new ProgramData();
            dat.setBits(bits.intValue());
            dat.setName(name);
            data_to_return.add(dat);
        }
        return data_to_return;
    }

    private boolean isValid(String name) {
        for (String s : AVOID_TEXTUAL) {
            if (name.trim().equals(s)) {
                return false;
            }
        }
        for (String s : AVOID_CONTAINS) {
            if (name.trim().contains(s)) {
                return false;
            }
        }
        return true;
    }
    private static final String LETTERS = "abcdefghijklmnopqrstuvwxyz";

    private int getMaxColum() {
        return LETTERS.length() + 1;
    }

    private int getColumn(String str) {
        if (str.length() < 1) {
            return OTHERS_COLUMNS;
        }
        String txt = str.toLowerCase().substring(0, 1);
        Integer num = LETTERS.lastIndexOf(txt);
        if (num >= 0) {
            return num;
        }
        return OTHERS_COLUMNS;
    }
}
